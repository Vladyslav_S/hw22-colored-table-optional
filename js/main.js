"use strict";

const table = document.createElement("table");
const tr = document.createElement("tr");
const td = document.createElement("td");
const style = document.createElement("style");
style.textContent =
  ".black .white{background-color: black} .white .black {background-color: black} ";

table.classList.add("white");
table.id = "table";

document.body.prepend(style);

td.style.width = "25px";
td.style.height = "25px";
td.style.border = "2px solid black";

for (let i = 0; i < 30; i++) {
  const tdClone = td.cloneNode();
  tdClone.classList.add("white");
  tr.append(tdClone);
}

for (let i = 0; i < 30; i++) {
  const trClone = tr.cloneNode(true);
  table.append(trClone);
}

document.body.addEventListener(
  "click",
  function (e) {
    if (e.target.tagName === "BODY") {
      document.getElementById("table").classList.toggle("black");
      document.getElementById("table").classList.toggle("white");
    } else {
      e.target.classList.toggle("black");
      e.target.classList.toggle("white");
    }
  },
  false
);

document.body.append(table);
